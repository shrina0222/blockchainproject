package blockchainproject;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class wallet {
    public PrivateKey privatekey;
    public PublicKey publickey;

    public HashMap<String, transactionOutput> UTXOs = new HashMap<String, transactionOutput>();


    public wallet() {
        generateKeyPair();
    }

    public void generateKeyPair() {
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance("ECDSA", "BC");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            ECGenParameterSpec ecgen = new ECGenParameterSpec("prime192v1");
            keygen.initialize(ecgen, random);
            KeyPair keypair = keygen.generateKeyPair();

            publickey = keypair.getPublic();
            privatekey = keypair.getPrivate();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public float getWallet() {
        float total = 0;
        for (Map.Entry<String, transactionOutput> item: blockchain.UTXOs.entrySet()) {
            transactionOutput UTXO = item.getValue();
            if (UTXO.checkCoin(publickey)) {
                UTXOs.put(UTXO.ID, UTXO);
                total += UTXO.value;
            }
        }
        return total;
    }

	public transaction transferCoins(PublicKey reciever, float value) {
		if(getWallet() < value) {
			System.out.println("No monies");
			return null;
		}

		ArrayList<transactionInput> inputs = new ArrayList<transactionInput>();
    
		float total = 0;
		for (Map.Entry<String, transactionOutput> item: UTXOs.entrySet()){
			transactionOutput UTXO = item.getValue();
			total += UTXO.value;
			inputs.add(new transactionInput(UTXO.ID));
			if(total > value) break;
		}
		
		transaction newTransaction = new transaction(publickey, reciever, value, inputs);
		newTransaction.createSignature(privatekey);
		
		for(transactionInput input: inputs){
			UTXOs.remove(input.transactionOutputID);
		}
		return newTransaction;
	}
	
}
