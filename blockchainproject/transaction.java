package blockchainproject;
import java.security.*;
import java.util.ArrayList;

public class transaction {

    public String transactionID; 
    public PublicKey sender;
    public PublicKey reciever;
    public float value;
    public byte[] signature;
    private static int sequence = 0;

    public ArrayList<transactionInput> inputs = new ArrayList<transactionInput>();
	public ArrayList<transactionOutput> outputs = new ArrayList<transactionOutput>();

    public transaction(PublicKey sender, PublicKey reciever, float value, ArrayList<transactionInput> inputs) {
        this.sender = sender;
        this.reciever = reciever;
        this.value = value;
        this.inputs = inputs;
    }

	private String getHash() {
        sequence++;
        String sender2 = stringHash.getStringFromKey(sender);
        String reciever2 = stringHash.getStringFromKey(reciever);
        String value2 = Float.toString(value);
        return stringHash.hashSha512(sequence + sender2 + reciever2 + value2);
    }

    public void createSignature(PrivateKey privatekey) {
        String data = stringHash.getStringFromKey(sender) + stringHash.getStringFromKey(reciever) + Float.toString(value);
        signature = stringHash.createECDSA(privatekey, data);
    }

    public boolean checkSignature() {
        String data = stringHash.getStringFromKey(sender) + stringHash.getStringFromKey(reciever) + Float.toString(value);
        return stringHash.checkECDSA(sender, data, signature);
    }

    public boolean doATransaction() {
        if (checkSignature() == false) {
            System.out.println("Transaction failed");
            return false;
        }

        for (transactionInput i : inputs) {
            i.UTXO = blockchain.UTXOs.get(i.transactionOutputID);
        }
   
        float remainder = getValueInput() - value;
        transactionID = getHash();
        outputs.add(new transactionOutput(this.reciever, value, transactionID));
        outputs.add(new transactionOutput(this.sender, remainder, transactionID));

        for (transactionOutput j : outputs) {
            blockchain.UTXOs.put(j.ID, j);
        }

        for (transactionInput k : inputs) {
            blockchain.UTXOs.remove(k.UTXO.ID);
        }
        
        return true;
    }

    public float getValueInput () {
        float total = 0;
        for (transactionInput i : inputs) {
            total += i.UTXO.value;
        }
        return total;
    }

    public float getValueOutput () {
        float total = 0;
        for (transactionOutput i : outputs) {
            total += i.value;
        }
        return total;
    }

}
