# Run from directory above blockchainproject

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/transactionOutput.java

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/transactionInput.java 

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/transaction.java     

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/stringHash.java 

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/block.java     

javac -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/blockchain.java

java -cp ".:blockchainproject/:blockchainproject/bcprov-jdk15on-159.jar" blockchainproject/blockchain
